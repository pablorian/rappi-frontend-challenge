import { Component, OnInit } from '@angular/core';
import { LoaderService } from "../../providers/loader/loader.service";
import { IProduct } from "../../interface/IProduct";
import { ActivatedRoute } from "@angular/router";
import {UserService} from "../../providers/user/user.service";

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.scss']
})
export class ProductosComponent implements OnInit {

  products: Array<IProduct>;
  isAlreadyAdded: any = (product) => {return this.user.isAlreadyAdded(product)};
  add: any =  (product) => this.user.addProduct(product);
  remove: any =  (product) => this.user.removeProduct(product);

  constructor(private route: ActivatedRoute, private user:UserService, private loader:LoaderService) { }

  ngOnInit() {
    this.loader.getFilter().subscribe( value => this.products = value);
    this.loader.filter();
  }

}
