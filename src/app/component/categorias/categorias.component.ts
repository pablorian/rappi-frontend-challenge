import { Component, OnInit } from '@angular/core';
import { LoaderService } from "../../providers/loader/loader.service";
import { ICategory } from "../../interface/ICategory";

@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.component.html',
  styleUrls: ['./categorias.component.scss']
})
export class CategoriasComponent implements OnInit {

  categories: Array<ICategory>;

  constructor(private loader:LoaderService) { }

  ngOnInit() {
    this.categories = this.loader.categories
  }

}
