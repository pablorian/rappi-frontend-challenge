import {Component, OnInit, DoCheck} from '@angular/core';
import { LoaderService } from "../../providers/loader/loader.service";

@Component({
  selector: 'app-filtros',
  templateUrl: './filtros.component.html',
  styleUrls: ['./filtros.component.scss']
})
export class FiltrosComponent implements OnInit, DoCheck {

  modelFilter: any;
  priceMin: string;
  priceMax: string;

  constructor(private loader:LoaderService ) { }

  ngOnInit() {
    this.modelFilter = this.loader.getModelFilter();
  }

  ngDoCheck(): void {
    this.loader.setFilterModel(this.modelFilter);
  }

  currencyInputChanged(value) {
    var num = value.replace(/[$,]/g, "");
    return Number(num);
  }

}
