import {Component, Input, OnInit} from '@angular/core';
import {ICategory} from "../../interface/ICategory";

@Component({
  selector: 'app-label-category',
  templateUrl: './label-category.component.html',
  styleUrls: ['./label-category.component.scss']
})
export class LabelCategoryComponent implements OnInit {

  @Input() category: ICategory;

  constructor() { }

  ngOnInit() {
  }

}
