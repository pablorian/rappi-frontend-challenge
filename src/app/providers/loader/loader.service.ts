import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { forkJoin, Observable, Subject } from "rxjs";
import { filter } from 'lodash'
import { IProduct } from "../../interface/IProduct";
import { ICategory } from "../../interface/ICategory";
import { ActivatedRoute } from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  private dataLoaded: Observable<any> = new Observable(this.loadedFunction.bind(this));
  private filterSubject: Subject<any> = new Subject<any>();
  private modelFilter: any = {available:"true", quantity:0, priceMin:0, priceMax:5000000};
  private categorySelected: number;
  products: Array<IProduct>;
  categories: Array<ICategory>;


  constructor(private route: ActivatedRoute, private http:HttpClient) {
    this.route
      .queryParams
      .subscribe((params) => {
        this.categorySelected = Number(params.id);
        this.filter()
      });
  }

  init():Observable<any> {
    return this.dataLoaded;
  }

  /*
  * I create this to only load the service once,
  * no care where you use the values.
   */
  loadedFunction(observer) {
    if(!this.products || !this.categories ){
      forkJoin([this.loadProducts(), this.loadCategories()]).subscribe((results) => {
        this.products = (results[0] as any).products;
        this.categories = (results[1] as any).categories;
        observer.next(true);
      });
    } else {
      observer.next(true);
    }
  }

  getFilter():Subject<any> {
    return this.filterSubject
  }

  getModelFilter() {
    return this.modelFilter
  }

  setFilterModel(modelFilter){
    this.modelFilter = this.modelFilter;
    this.filter()
  }

  filter() {
    let products = filter(this.products, {sublevel_id:this.categorySelected, available:(this.modelFilter.available === 'true')});
    products = filter(products, (value) => {
      let price = value.price.replace(/[$,]/g, "");
      return price >= this.modelFilter.priceMin &&
      price <=  this.modelFilter.priceMax
    });
    products = filter(products, (value) => {
      return value.quantity >= this.modelFilter.quantity
    })
    this.filterSubject.next(products);
  }

  loadProducts(){
    return this.http.get('assets/products.json');
  }

  loadCategories(){
    return this.http.get('assets/categories.json');
  }

  getProductsByCategory(id){
    return filter(this.products, {sublevel_id:Number(id)});
  }


}
