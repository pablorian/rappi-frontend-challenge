import { Injectable } from '@angular/core';
import { Subject } from "rxjs";
import { IProduct } from "../../interface/IProduct";
import { find, filter } from 'lodash'

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private productsAdded:Array<IProduct>;
  private productSubject: Subject<any> = new Subject<any>();

  constructor() {
    this.productsAdded = JSON.parse(localStorage.getItem('userProduct')) || new Array<IProduct>();
  }

  getProductSubject(): Subject<any> {
    return this.productSubject;
  }

  addProduct(product:IProduct){
    if(!this.isAlreadyAdded(product)){
      this.productsAdded.push({...product});
      this.save()
    }
  }

  removeProduct(product:IProduct){
    this.productsAdded = filter(this.productsAdded, x => x.id !== product.id );
    this.save()
  }

  save(){
    localStorage.setItem('userProduct', JSON.stringify(this.productsAdded));
    this.productSubject.next(this.productsAdded);
  }

  isAlreadyAdded(product:IProduct) {
    return find(this.productsAdded, {id:product.id})
  }

}
