import { Component, OnInit } from '@angular/core';
import {IProduct} from "../interface/IProduct";
import {UserService} from "../providers/user/user.service";

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.component.html',
  styleUrls: ['./carrito.component.scss']
})
export class CarritoComponent implements OnInit {

  products: Array<IProduct>;
  remove: any =  (product) => this.user.removeProduct(product);

  constructor(private user:UserService) { }

  ngOnInit() {
    this.user.getProductSubject().subscribe(value => this.products = value);
    this.user.save();
  }

}
