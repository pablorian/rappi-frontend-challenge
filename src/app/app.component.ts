import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import {LoaderService} from "./providers/loader/loader.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  serviceLoaded: boolean = false;

  constructor( private loader:LoaderService) {

  }

  ngOnInit() {
    this.loader.init().subscribe(value => {
      this.serviceLoaded = value;
    })


  }

}
