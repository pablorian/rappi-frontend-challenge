export interface ICategory {
  id: number;
  name: string;
  sublevels: Array<ICategory>;
}
