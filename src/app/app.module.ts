import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoaderService } from "./providers/loader/loader.service";
import { ProductosComponent } from './component/productos/productos.component';
import { CategoriasComponent } from './component/categorias/categorias.component';
import { FiltrosComponent } from './component/filter/filtros.component';
import { HttpClientModule} from "@angular/common/http";
import { LabelCategoryComponent } from './component/label-category/label-category.component';
import { FormsModule } from "@angular/forms";
import {UserService} from "./providers/user/user.service";
import { CarritoComponent } from './carrito/carrito.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductosComponent,
    CategoriasComponent,
    FiltrosComponent,
    LabelCategoryComponent,
    CarritoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    FormsModule
  ],
  providers: [
    LoaderService,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
